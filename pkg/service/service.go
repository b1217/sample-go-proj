package service

type Repository interface {
	FetchOne(id int)
}

type Service struct {
	Repo Repository
}

func (s Service) Do() {}
