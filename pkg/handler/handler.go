package handler

import (
	"fmt"
	"net/http"
	"time"
)

type Service interface {
	Do()
}

func New(svc Service) *hdlr {
	return &hdlr{service: svc}
}

type hdlr struct {
	service Service
}

func (h hdlr) Handle(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("Handling request at %s\n", time.Now())
	fmt.Println("Hello", r.URL.Path)
}
