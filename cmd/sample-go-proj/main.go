package main

import (
	"database/sql"
	"fmt"
	"net/http"

	"gitlab.com/b1217/sample-go-proj/pkg/handler"
	"gitlab.com/b1217/sample-go-proj/pkg/repository"
	"gitlab.com/b1217/sample-go-proj/pkg/service"
)

func main() {
	db, err := sql.Open("postgres", "postgresql://")
	if err != nil {
		panic(err)
	}
	repo := repository.Repository{DB: db}
	svc := service.Service{Repo: repo}
	hdlr := handler.New(svc)
	http.HandleFunc("/", hdlr.Handle)
	http.Handle("/favicon.ico", http.NotFoundHandler())
	http.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Health check")
		w.WriteHeader(http.StatusOK)
	})
	fmt.Println("Listening on port 9090")
	http.ListenAndServe(":9090", nil)
}
